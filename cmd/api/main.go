package main

import (
	"bytes"
	"context"
	"database/sql"
	"fmt"
	"image"
	"image/jpeg"
	"log"
	"os"

	_ "github.com/microsoft/go-mssqldb"
	. "github.com/spf13/viper"
)

var (
	db *sql.DB
)

func init() {
	// binds env variables
	SetEnvPrefix("wap") // will be uppercased automatically
	BindEnv("user")
	BindEnv("password")

	// config file
	AddConfigPath("./")
	SetConfigName("config")
	SetConfigType("yaml")
}

func main() {

	// gets login and password from env variables
	user := Get("user")
	password := Get("password")

	// to aviod password with nil value
	if password == nil {
		password = ""
	}

	// check if configfile is available
	if err := ReadInConfig(); err != nil {
		log.Println("No configuration file loaded")
		log.Fatal(err)
	}

	// gets variables from config file
	server := GetString("server")
	port := GetInt("port")
	database := GetString("database")

	// https://docs.microsoft.com/ru-ru/azure/azure-sql/database/connect-query-go?view=azuresql#insert-code-to-query-the-database
	// Build connection string
	connString := fmt.Sprintf("server=%s;user id=%s;password=%s;port=%d;database=%s;",
		server, user, password, port, database)

	var err error

	// Create connection pool
	db, err = sql.Open("sqlserver", connString)
	if err != nil {
		log.Fatal("Error creating connection pool: ", err.Error())
	}
	ctx := context.Background()
	err = db.PingContext(ctx)
	if err != nil {
		log.Fatal(err.Error())
	}
	fmt.Print("Connected!\n")

	// Reads furniture connected with ID task
	if err := ReadIDTable(); err != nil {
		log.Fatal("Error reading table: ", err.Error())
	}
	fmt.Print("Read table successfully.\n")

	// Reads ID task
	if err := ReadIDTask(); err != nil {
		log.Fatal("Error reading Order: ", err.Error())
	}
	fmt.Print("Read task id successfully.\n")

	// Reads drawing
	if err := ReadDrawing(); err != nil {
		log.Fatal("Error reading drawing: ", err.Error())
	}
	fmt.Print("Read drawing successfully.\n")
}

// ReadIDTable reads all of furniture connected with order id
func ReadIDTable() error {
	ctx := context.Background()

	// Check if database is alive.
	if err := db.PingContext(ctx); err != nil {
		return err
	}

	tsql := fmt.Sprintf(`
select
  nCount,
  MatName,
  ConturNum
from
  dbo.f_GetListFurnit_Contur(%d, 0)
order by
  ConturNum,
  Art,
  MatName
`, 1000501352)

	// Execute query
	rows, err := db.QueryContext(ctx, tsql)
	if err != nil {
		return err
	}
	defer rows.Close()

	// Iterate through the result set.
	for rows.Next() {
		var nCount, matName, conturNum string
		var count int

		// Get values from row.
		err := rows.Scan(&nCount, &matName, &conturNum)
		if err != nil {
			return err
		}

		fmt.Printf("nCount: %s, MatName: %s, ConturNum: %s\n", nCount, matName, conturNum)
		count++
	}

	switch {
	case err == sql.ErrNoRows:
		log.Printf("no order with BarCode %d\n", 1000501352)
	case err != nil:
		log.Fatalf("query error: %v\n", err)
	default:
		log.Printf("IDTable:\n%s\n", "!")
	}

	return nil
}

// ReadIDTask reads some record
func ReadIDTask() error {
	ctx := context.Background()

	// Check if database is alive.
	if err := db.PingContext(ctx); err != nil {
		return err
	}

	tsql := fmt.Sprintf(`
select
  top(1) Project.idTask
from
  BarCode
  inner join WriteMater on BarCode.idWriteMater = WriteMater.ID
  inner join Project on WriteMater.idProject = Project.ID
where
  BarCode.BarCode = '1000501352'
`)
	var result int

	// Execute query
	err := db.QueryRowContext(ctx, tsql).Scan(&result)
	switch {
	case err == sql.ErrNoRows:
		log.Printf("no order with BarCode %d\n", 1000501352)
	case err != nil:
		log.Fatalf("query error: %v\n", err)
	default:
		log.Printf("idTask is %d\n", result)
	}

	return nil
}

func ReadDrawing() error {
	ctx := context.Background()

	// Check if database is alive.
	if err := db.PingContext(ctx); err != nil {
		return err
	}

	tsql := fmt.Sprintf(`
Select 
  Plot
from 
  Plot 
where 
  idProject = 2106603 
  and nGroup = 1 
  and isNull(bGPPlot, 0) = 0
`)
	var result []byte

	// Execute query
	err := db.QueryRowContext(ctx, tsql).Scan(&result)
	switch {
	case err == sql.ErrNoRows:
		log.Printf("no order with idProject %d\n", 2106603)
	case err != nil:
		log.Fatalf("query error: %v\n", err)
	default:
		log.Printf("idTask is %b\n", result)
	}

	serveFrames(result)
	return nil
}

func serveFrames(imgByte []byte) {

	img, _, err := image.Decode(bytes.NewReader(imgByte))
	if err != nil {
		log.Fatalln(err)
	}

	out, _ := os.Create("./img.jpeg")
	defer out.Close()

	var opts jpeg.Options
	opts.Quality = 100

	err = jpeg.Encode(out, img, &opts)
	//jpeg.Encode(out, img, nil)
	if err != nil {
		log.Println(err)
	}

}
